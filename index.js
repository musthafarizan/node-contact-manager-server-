const express = require('express');

const app = express();

app.use(express.json());

const PORT = process.env.PORT || 3300;

app.use('/api', (req, res) => {
  res.json({
    app: 'contact manager',
    version: '1.0.0-alpha',
    apiVersion: '1.0.0-alpha'
  });
});

app.use('/', (req, res) => {
  res.send(`
  <h1>The Home Page</h1>
  `);
});

app.listen(PORT, () => console.log(`app is listing to on port ${PORT}`));
